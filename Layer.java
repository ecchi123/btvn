import java.util.ArrayList;
import java.util.List;

public class Layer {
    private List<Shape> shapes = new ArrayList<>();
    public void addShape(Shape shape){
        shapes.add(shape);
    }
    public void removeCircles(){
        int size = shapes.size();
        for (int i = 0; i < size; i++){
            if (shapes.get(i) instanceof Circle){
                shapes.remove(shapes.get(i));
                size--;
                i--;
            }
        }
    }
    public String getInfo(){
        String information = "Layer of crazy shapes: " + "\n";
        int size = shapes.size();
        for (int i = 0; i < size; i++){
            information += shapes.get(i).toString() + "\n";
        }
        information.trim();
        return information;
    }
    public void removeDuplicates(){
        for (int i = 0; i < shapes.size(); i++){
            for (int j = shapes.size() - 1; j > i; j--){
                if (shapes.get(j).equals(shapes.get(i))){
                    shapes.remove(j);
                }
            }
        }
    }
}
