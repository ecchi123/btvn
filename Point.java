public class Point {
    private double x;
    private double y;
    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }
    public double getX(){
        double _x = (double)Math.round(x * 10) / 10;
        return _x;
    }

    public double getY() {
        double _y = (double)Math.round(y * 10) / 10;
        return _y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
    public double distance(Point other){
        return Math.sqrt(Math.pow(x - other.getX(), 2)+ Math.pow(y - other.getY(), 2));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point){
            Point p = (Point)obj;
            return (x - p.x <= 0.001 && y - p.y <= 0.001);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "(" + this.getX() + "," + this.getY() + ")";
    }
}