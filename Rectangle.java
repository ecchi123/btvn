import java.math.BigDecimal;

public class Rectangle extends Shape{
    protected double width;
    protected double length;
    protected Point topLeft;
    public Rectangle(){

    }
    public Rectangle(double width, double length){
        if (width > 0){
            this.width = width;
        }
        if (length > 0) {
            this.length = length;
        }
    }
    public Rectangle(double width, double length, String color, boolean filled){
        super(color, filled);
        if (width > 0){
            this.width = width;
        }
        if (length > 0) {
            this.length = length;
        }
    }
    public Rectangle(Point topLeft, double width, double length, String color, boolean filled){
        super(color, filled);
        this.width = width;
        this.length = length;
        this.topLeft = topLeft;
    }
    public double getWidth() {
        double _width = (double)Math.round(width * 10) / 10;
        return _width;
    }

    public double getLength() {
        double _length = (double)Math.round(length * 10) / 10;
        return _length;
    }

    public void setWidth(double width) {
        if (width > 0){
            this.width = width;
        }
    }

    public void setLength(double length) {
        if (length > 0){
            this.length = length;
        }
    }

    public double getArea(){
        BigDecimal _width = new BigDecimal(Double.toString(width));
        BigDecimal _length = new BigDecimal(Double.toString(length));
        BigDecimal result = _length.multiply(_width);
        return result.doubleValue();
    }

    public double getPerimeter() {
        BigDecimal _width = new BigDecimal(Double.toString(width));
        BigDecimal _length = new BigDecimal(Double.toString(length));
        BigDecimal result = _length.add(_width);
        return (2 * result.doubleValue());
    }
    public Point getTopLeft() {
        return topLeft;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Rectangle){
            Rectangle other = (Rectangle)obj;
            return (width - other.width <= 0.001 && length - other.length <= 0.001 && topLeft.equals(other.topLeft));
        }
        return false;
    }

    public String toString() {
        String result = "Rectangle[" + "topLeft=" + topLeft.toString() + "," + "width=" + width + "," + "length=" + length + "," + "color=" + color + "," +
                "filled=" + filled + "]";
        return result;
    }
}