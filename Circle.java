public class Circle extends Shape{
    protected double radius;
    protected Point center;
    public Circle(){

    }
    public Circle(double radius){
        if (radius > 0){
            this.radius = radius;
        }
    }
    public Circle(double radius, String color, boolean filled){
        super(color, filled);
        if (radius > 0){
            this.radius = radius;
        }
    }
    public Circle(Point center, double radius, String color, boolean filled){
        super(color, filled);
        this.radius = radius;
        this.center = center;
    }
    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public double getRadius() {
        double _radius = (double)Math.round(radius * 10) / 10;
        return _radius;
    }

    public void setRadius(double radius) {
        if (radius > 0){
            this.radius = radius;
        }
    }
    public double getArea(){
        return (Math.PI * radius * radius);
    }
    public double getPerimeter(){
        return (2 * Math.PI * radius);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Circle){
            Circle other = (Circle) obj;
            return (radius - other.radius <= 0.001 && center.equals(other.center));
        }
        return false;
    }

    public String toString(){
        String result = "Circle[" + "center=" + center.toString() + "," + "radius=" + this.getRadius() + "," + "color=" + color + "," + "filled=" + filled + "]";
        return result;
    }
}
