public class Square extends Rectangle{
    public Square(){}
    public Square(double side){
        super(side, side);
    }
    public Square(double side, String color, boolean filled){
        super(side, side, color, filled);
    }
    public Square(Point topLeft, double side, String color, boolean filled){
        super(topLeft, side, side, color, filled);
    }
    public double getSide(){
        return super.getLength();
    }
    public void setSide (double side){
        if (side > 0){
            super.length = side;
            super.width = side;
        }
    }
    public void setWidth(double side){
        if (side > 0){
            super.width = side;
            super.length = side;
        }
    }
    public void setLength(double side){
        if (side > 0) {
            super.length = side;
            super.width = side;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Square){
            Square sq = (Square)obj;
            return (this.getSide() - sq.getSide() <= 0.001 && this.topLeft.equals(sq.topLeft));
        }
        return false;
    }

    public String toString(){
        String result = "Square[" + "topLeft=" + super.topLeft.toString() + "," + "side=" + this.getSide() + "," + "color=" + color + "," + "filled=" + filled + "]";
        return result;
    }
}